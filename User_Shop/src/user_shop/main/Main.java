package user_shop.main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Main  extends JavaPlugin
{
	public static JavaPlugin mother;
	public static Config config;
	
	@Override
	public void onEnable(){
		Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "Laylia의 유저 샵 활성화!");
		Bukkit.getPluginManager().registerEvents(new Events(),  this);
		mother = this;
		config = new Config();
	}
	
	@Override
	public void onDisable()
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.GRAY + "Laylia의 유저 샵 b활성화..");
	}
	
	@Override
	 public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
		return false;
    }
}
