package user_shop.shop;

import org.bukkit.inventory.ItemStack;

/*
 * 2017.1.2 Laylia_Sweet
 * 상점의 총체적인 데이터 테이블을 관리
 */
public class Shop_Element
{
	public String Owner_ID; // 오너의 ID
	public String Owner_UUID; // 오너의 UUID
	public double balance; // 상점의 잔고

	public ItemStack[] items; // 저장소의 아이템
	public long close_milisecond; // 문 닫는 시간을 알려주는 millisecond
	
	
	
	// 상점 문 닫을 때가 됐나 체크하기
	public void Check_Closing_Time(long now_time)
	{
		if(now_time > close_milisecond)
		{
			//닫기 처리
		}
	}
	
	// 상점에서 팔고 있는 아이템만 추린다
	public ItemStack[] Get_Selling_Items() 
	{
		return null;
	}
	
	 // 상점에서 사고 있는 아이템만 추린다
	public ItemStack[] Get_Buying_Items()
	{
		return null;
	}
}
