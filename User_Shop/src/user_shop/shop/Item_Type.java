package user_shop.shop;

public enum Item_Type
{
	SELL, // 상점에서 파는 아이템
	BUY, // 상점에서 사는 아이템
	SELL_CART, // 상점에게 사는 아이템
	BUY_CART, // 상점에게 파는 아이템
	UNKNOWN // 알 수 없음
}
