package user_shop.shop;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/*
 * 2017.1.2 Laylia_Sweet
 * 상점에서 파는 아이템의 정보를 관리
 * 직접적 관여는 안하고, 구매 판매인지, 카트에 있는지 뭐 그런 것들을 판별해주는 역할만 한다
 */
public class Item_Info
{

	// 상점에서 파는 아이템인지 체크한다
	public static Item_Type Check_Item_Type(ItemStack item)
	{
		if(item != null)
		{
			if(item.hasItemMeta())
			{
				ItemMeta meta = item.getItemMeta();
				if(meta.hasLore())
				{
					for(String lore : meta.getLore())
					{
						if(lore.contains("[ 구매중 ]"))
						{
							
						}
					}
				}
			}
		}
		return Item_Type.UNKNOWN;
	}
}
